import 'dart:wasm';

import 'package:myGPA/Subject.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SubjectDb {
  static Future<Database> init() async {
    return await openDatabase(join(await getDatabasesPath(), 'sub.db'),
        onCreate: (db, version) {
      return db.execute(
          "CREATE TABLE subject(id INTEGER PRIMARY KEY , name TEXT, code TEXT, credit TEXT, gpv TEXT,grade TEXT)");
    }, version: 1);
  }

  static Future<void> InsertGrade(Subject subject, Database db) async {
    db.insert('subject', subject.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  static Future<List<Subject>> getSubjects(Database db) async {
    final List<Map<String, dynamic>> maps = await db.query('subject');
    return List.generate(maps.length, (i) {
      return Subject(
          id: maps[i]['id'],
          name: maps[i]['name'],
          gpv: maps[i]['gpv'],
          grade: maps[i]['grade'],
          credit: maps[i]['credit'],
          code: maps[i]['code']);
    });
  }

  static Future<void> delete(Database db, int id) async {
    await db.delete('subject', where: 'id = ?', whereArgs: [id]);
  }

  static Future<Subject> updateSubject(Database db, Subject subject) async {
    await db.update('subject', subject.toMap(),
        where: 'id = ?', whereArgs: [subject.id]);
  }
}
