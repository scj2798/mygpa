class Subject {
  int id;
  String code;
  String name;
  String credit;
  String grade;
  String gpv;

  Subject({this.id, this.name, this.code, this.grade, this.gpv, this.credit});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'code': code,
      'grade': grade,
      'gpv': gpv,
      'credit': credit
    };
  }
}
