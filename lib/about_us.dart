import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  String packVersion = "0.0.0";
  String packBuildNumber;

  @override
  Widget build(BuildContext context) {
    PackageInfo.fromPlatform().then((pInfo) {
      setState(() {
        packVersion = pInfo.version;
        packBuildNumber = pInfo.buildNumber;
      });
    });

    print("$packVersion");
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage("assets/images/a.png"))),
          child: Column(
            children: [
              SizedBox(height: MediaQuery.of(context).size.height * 0.2),
              Icon(Icons.equalizer, size: 40.0, color: Colors.blue),
              SizedBox(height: MediaQuery.of(context).size.height * 0.01),
              Text("myGPA",
                  style: TextStyle(fontSize: 40, color: Color(0xFF011C63))),
              SizedBox(height: MediaQuery.of(context).size.height * 0.05),
              Text("${packVersion}.${packBuildNumber}",
                  style: TextStyle(fontSize: 16, color: Color(0xFF011C63))),
              Text("version",
                  style: TextStyle(fontSize: 12, color: Color(0xFF011C63))),
              SizedBox(height: MediaQuery.of(context).size.height * 0.05),
              Text("developed by",
                  style: TextStyle(fontSize: 8, color: Color(0xFF011C63))),
              SizedBox(height: MediaQuery.of(context).size.height * 0.01),
              ClipOval(
                  child: Image.asset("assets/images/my.jpg",
                      height: MediaQuery.of(context).size.height * 0.08,
                      width: MediaQuery.of(context).size.height * 0.08,
                      fit: BoxFit.cover)),
              SizedBox(height: MediaQuery.of(context).size.height * 0.01),
              Text("Chathuranga Jayawardana",
                  style: TextStyle(fontSize: 14, color: Color(0xFF011C63))),
              SizedBox(height: MediaQuery.of(context).size.height * 0.02),
              Text("SCJ101",
                  style: TextStyle(fontSize: 12, color: Color(0xFF011C63))),
              SizedBox(height: MediaQuery.of(context).size.height * 0.04),
              Text("All Rights Reserved.",
                  style: TextStyle(fontSize: 8, color: Color(0xFF011C63))),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }
}
