import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myGPA/Subject.dart';
import 'package:sqflite/sqflite.dart';
import 'db.dart';

class NewSubjectView extends StatefulWidget {
  Subject sub;
  bool isUpdate;

  NewSubjectView({this.sub, this.isUpdate});

  @override
  _NewSubjectViewState createState() =>
      _NewSubjectViewState(sub: sub, isUpdate: isUpdate);
}

class _NewSubjectViewState extends State<NewSubjectView> {
  Subject sub;
  bool isUpdate;

  _NewSubjectViewState({this.sub, this.isUpdate});
  // ignore: non_constant_identifier_names

  String subject_code;
  // ignore: non_constant_identifier_names
  String subject_name;
  // ignore: non_constant_identifier_names
  String subject_gpv;
  // ignore: non_constant_identifier_names
  String subject_credit;
  // ignore: non_constant_identifier_names
  String subject_grade;

  // ignore: non_constant_identifier_names
  String select_val = "Select Grade";

  // ignore: non_constant_identifier_names
  Database db_;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (isUpdate) {
      subject_code = sub.code;
      subject_credit = sub.credit;
      subject_gpv = sub.gpv;
      subject_name = sub.name;
      subject_grade = select_val = sub.grade;
    }
  }

  @override
  Widget build(BuildContext context) {
    SubjectDb.init().then((dbV) {
      db_ = dbV;
    });

    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(24.0, 0, 24.0, 12.0),
              child: TextField(
                  decoration: InputDecoration(
                    labelText: "Name",
                    hintText: (isUpdate) ? subject_name : null,
                    labelStyle: TextStyle(color: Color(0xFF011C63)),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                  onChanged: (txt) {
                    subject_name = txt;
                  }),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(24.0, 0, 24.0, 12.0),
              child: TextField(
                decoration: InputDecoration(
                    hintText: (isUpdate) ? subject_code : null,
                    labelText: "Code",
                    labelStyle: TextStyle(color: Color(0xFF011C63)),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        borderSide: BorderSide(color: Colors.black)),
                    hintStyle: TextStyle(color: Color(0xFF011C63))),
                onChanged: (txt) {
                  subject_code = txt;
                },
              ),
            ),
            Container(
              padding: EdgeInsets.fromLTRB(24.0, 0, 24.0, 12.0),
              child: TextField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: (isUpdate) ? subject_credit : null,
                  labelText: "Credit",
                  labelStyle: TextStyle(color: Color(0xFF011C63)),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                ),
                onChanged: (txt) {
                  subject_credit = txt.toString();
                  if (isUpdate) {
                    subject_gpv = (double.parse(subject_credit != null
                                ? subject_credit
                                : "0.0001") *
                            Grade.FindGPV(
                                select_val != null ? select_val : "Z"))
                        .toStringAsFixed(1);
                  }
                },
              ),
            ),
            Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.fromLTRB(24.0, 0, 24.0, 12.0),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.0),
                      border: Border.all(color: Colors.black38)),
                  child: new DropdownButton(
                      key: Key('drp_btn'),
                      hint: Text(
                        select_val,
                        style: TextStyle(color: Color(0xFF011C63)),
                      ),
                      items: Grade.glist.map((grd) {
                        return DropdownMenuItem(
                            child: Text(
                              grd,
                              style: TextStyle(color: Color(0xFF011C63)),
                            ),
                            value: grd);
                      }).toList(),
                      onChanged: (txt) {
                        setState(() {
                          select_val = txt;
                          subject_grade = select_val.toString();
                          subject_gpv = (double.parse(subject_credit != null
                                      ? subject_credit
                                      : "0.0001") *
                                  Grade.FindGPV(
                                      select_val != null ? select_val : "Z"))
                              .toStringAsFixed(1);
                        });
                        print(select_val);
                      }),
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.all(4.0),
                  child: GestureDetector(
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 12.0, horizontal: 20.0),
                        child: Text("Submit",
                            style:
                                TextStyle(color: Colors.white, fontSize: 14)),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(24.0),
                            color: Color(0xFF011C63)),
                      ),
                      onTap: () {
                        setState(() {
                          if (subject_name != null &&
                              subject_credit != null &&
                              subject_gpv != null &&
                              subject_grade != null &&
                              subject_code != null) {
                            if (isUpdate) {
                              var s = Subject(
                                  id: sub.id,
                                  name: subject_name != null
                                      ? subject_name
                                      : "NO NAME",
                                  code: subject_code != null 
                                      ? subject_code
                                      : "NO CODE",
                                  grade: subject_grade != null
                                      ? subject_grade
                                      : "NO GRAGE",
                                  gpv: subject_gpv != null
                                      ? subject_gpv
                                      : "0.00",
                                  credit: subject_credit != null
                                      ? subject_credit
                                      : "0.00");
                              print(s);
                              SubjectDb.updateSubject(db_, s);
                            } else {
                              var s = Subject(
                                  id: new DateTime.now().microsecond,
                                  name: subject_name != null
                                      ? subject_name
                                      : "NO NAME",
                                  code: subject_code != null
                                      ? subject_code
                                      : "NO CODE",
                                  grade: subject_grade != null
                                      ? subject_grade
                                      : "NO GRAGE",
                                  gpv: subject_gpv != null
                                      ? subject_gpv
                                      : "0.00",
                                  credit: subject_credit != null
                                      ? subject_credit
                                      : "0.00");

                              SubjectDb.InsertGrade(s, db_);
                            }
                          }
                          print("DONE");
                        });
                        Navigator.of(context).pop();
                      }),
                ),
                Container(
                    padding: EdgeInsets.all(4.0),
                    child: FlatButton(
                      padding: EdgeInsets.all(12.0),
                      child: Text(
                        'Cancel',
                        style: TextStyle(color: Color(0xFF011C63)),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  void setState(fn) {
    super.setState(fn);
  }
}

class Grade {
  String grade;
  double val;
  static List<String> glist = [
    "A+",
    "A",
    "A-",
    "B+",
    "B",
    "B-",
    "C+",
    "C",
    "C-",
    "D+",
    "D",
    "D-"
  ];

  // ignore: non_constant_identifier_names
  static double FindGPV(String grade) {
    switch (grade) {
      case "A+":
        return 4.0;
        break;
      case "A":
        return 4.0;
        break;
      case "A-":
        return 3.7;
        break;
      case "B+":
        return 3.3;
        break;
      case "B":
        return 3;
        break;
      case "B-":
        return 2.7;
        break;
      case "C+":
        return 2.3;
        break;
      case "C":
        return 2.0;
        break;
      case "C-":
        return 1.7;
        break;
      case "D+":
        return 1.3;
        break;
      case "D":
        return 1;
        break;
      default:
        return 0.0;
        break;
    }
  }
}
