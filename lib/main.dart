import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:myGPA/Subject.dart';
import 'package:myGPA/about_us.dart';
import 'package:myGPA/db.dart';
import 'package:sqflite/sqflite.dart';
import 'new_subject_view.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'myGPA',
      home: HomeState(),
      theme: ThemeData(
        primaryColor: Colors.indigo,
      ),
    );
  }
}

class HomeState extends StatefulWidget {
  @override
  _HomeStateState createState() => _HomeStateState();
}

class _HomeStateState extends State<HomeState> {
  Database db;
  double gpa = 0.00;
  double tCredit = 0.00;
  double tGpv = 0.00;
  int tSub = 0;
  // ignore: non_constant_identifier_names
  double progressPrec = 0.0;
  List<SubjectGPA> data = [];
  List<charts.Series<SubjectGPA, int>> series = [];
  List<Subject> list = [];
  int selectView = 0;

//  List<SubjectGPA> data = [
//    SubjectGPA(subject_code: 0,gpa: 25),
//    SubjectGPA(subject_code: 1,gpa: 30),
//    SubjectGPA(subject_code: 3,gpa: 80),
//    SubjectGPA(subject_code: 4,gpa: 40),
//    SubjectGPA(subject_code: 5,gpa: 10)
//  ];

//  List<Subject> list = [
//    Subject(id:12,name:'SUB A ', code: 'CSC890',grade:'A', credit:'15.23',gpv:'25'),
//    Subject(id:12,name:'SUB B ', code: 'CSC8985',grade:'B', credit:'185.23',gpv:'58'),
//    Subject(id:13,name:'SUB C ', code: 'CSC8d985',grade:'B+', credit:'1885.23',gpv:'68'),
//    Subject(id:13,name:'SUB C ', code: 'CSC8d985',grade:'B+', credit:'1885.23',gpv:'68')
//  ];

  calGPA() {
    tSub = 0;
    tCredit = tGpv = gpa = 0.00;
    tSub = list.length;
    if (list.length > 0) {
      list.forEach((sub) {
        tGpv += double.parse(sub.gpv);
        tCredit += double.parse(sub.credit);
        gpa = (tGpv / tCredit);
      });
    }
    progressPrec = (gpa / 4.0);
  }

  addData() {
    int a = 0;

    if (data.length > 0) data.clear();

    list.forEach((element) {
      data.add(SubjectGPA(subject_code: a++, gpa: double.parse(element.gpv)));
    });

    series = [
      new charts.Series<SubjectGPA, int>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (SubjectGPA sales, _) => sales.subject_code,
        measureFn: (SubjectGPA sales, _) => sales.gpa,
        data: data,
      )
    ];
  }

  // ignore: non_constant_identifier_names
  Widget CourceCard(Subject sub) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: Colors.black12, offset: Offset(0, 4), blurRadius: 10)
          ],
        ),
        child: Stack(
          children: [
            Positioned(
                child: Opacity(
                    opacity: 0.05,
                    child: ClipPath(
                      clipper: CardCliper(),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.2,
                        foregroundDecoration: BoxDecoration(
                            color: Colors.blueAccent,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black,
                                  offset: Offset(0, 12),
                                  blurRadius: 10)
                            ],
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(12.0),
                                topRight: Radius.circular(12.0))),
                      ),
                    ))),
            Container(
              padding: EdgeInsets.all(12.0),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(4.0),
                    child: Icon(Icons.assignment_outlined, color: Colors.blue),
                  ),
                  Container(
                    padding: EdgeInsets.all(4.0),
                    child: Text(sub.name,
                        style:
                            TextStyle(fontSize: 20, color: Color(0xFF011C63))),
                  ),
                  Container(
                    padding: EdgeInsets.all(4.0),
                    child: Text(sub.code,
                        style:
                            TextStyle(fontSize: 12, color: Color(0xFF011C63))),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        padding: EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12.0),
                            border: Border.all(color: Colors.blue, width: 1.5)),
                        child: Column(
                          children: [
                            Text(sub.grade,
                                style: TextStyle(
                                    fontSize: 24, color: Color(0xFF011C63))),
                            Text('Grade',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xFF011C63))),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            Text(sub.credit,
                                style: TextStyle(
                                    fontSize: 24, color: Color(0xFF011C63))),
                            Text('Credit',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xFF011C63))),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            Text(sub.gpv,
                                style: TextStyle(
                                    fontSize: 24, color: Color(0xFF011C63))),
                            Text('GPV',
                                style: TextStyle(
                                    fontSize: 12, color: Color(0xFF011C63))),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Positioned(
              top: 4.0,
              right: 4.0,
              child: Row(
                children: [
                  IconButton(
                      icon: Icon(Icons.edit),
                      onPressed: () {
                        showDialog(
                            context: context,
                            useSafeArea: true,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return SimpleDialog(
                                title: Text(
                                  'Update Subject ',
                                  style: TextStyle(color: Color(0xFF011C63)),
                                ),
                                children: [
                                  NewSubjectView(
                                    isUpdate: true,
                                    sub: sub,
                                  )
                                ],
                              );
                            });
                      }),
                  IconButton(
                    icon: Icon(Icons.close),
                    onPressed: () {
                      setState(() {
                        SubjectDb.delete(db, sub.id);
                      });
                    },
                  ),
                ],
              ),
            )
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    addData();

    SubjectDb.getSubjects(db).then((value) {
      print(value.length);
      setState(() {
        if (value.length > 0)
          list = value;
        else
          list = [];
        calGPA();
      });
    }).catchError((onError) => print(onError));

    return Scaffold(
      body: SingleChildScrollView(
          child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/a.png"),
                alignment: Alignment.bottomCenter,
                fit: BoxFit.fitHeight)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
                child: ClipPath(
              clipper: MyCliper(),
              child: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Color(0xFF011C63), Colors.blue],
                  ),
                ),
                width: MediaQuery.of(context).size.width * 1,
                height: MediaQuery.of(context).size.height * 0.3,
                child: Column(
                  children: [
                    SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8.0, vertical: 8.0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(
                                    Icons.equalizer_outlined,
                                    color: Colors.white,
                                    size: 16.0,
                                  ),
                                  SizedBox(width: 5),
                                  Text("myGPA",
                                      style: TextStyle(color: Colors.white)),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                              width: MediaQuery.of(context).size.width * 0.5),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 8.0),
                                child: GestureDetector(
                                  onTap: () {
                                    Navigator.push(context,
                                        MaterialPageRoute(builder: (context) {
                                      return AboutUs();
                                    }));
                                  },
                                  child: Icon(
                                    Icons.menu,
                                    color: Colors.white,
                                  ),
                                )),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                        height: MediaQuery.of(context).size.height * 0.003),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: Column(
                          children: [
                            Text("GPA",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 12.0)),
                            Text(gpa.toStringAsFixed(2),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 64.0))
                          ],
                        ))
                  ],
                ),
              ),
            )),
            Container(
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.7,
                child: DefaultTabController(
                  length: 2,
                  initialIndex: selectView,
                  child: TabBarView(
                    dragStartBehavior: DragStartBehavior.start,
                    children: [
                      Column(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(
                                vertical: 12.0, horizontal: 0.0),
                            padding: EdgeInsets.symmetric(
                                vertical: 8.0, horizontal: 0.0),
                            child: Text("Overview",
                                style: TextStyle(
                                    fontSize: 16, color: Color(0xFF011C63))),
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Container(
                                  padding: EdgeInsets.all(24.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12.0),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black12,
                                          offset: Offset(0, 3),
                                          blurRadius: 10)
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Icon(
                                        Icons.show_chart,
                                        color: Colors.blue,
                                      ),
                                      Text(
                                        tCredit.toStringAsFixed(2),
                                        style: TextStyle(
                                            fontSize: 36,
                                            color: Color(0xFF011C63)),
                                      ),
                                      Text(
                                        'Credits',
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Color(0xFF011C63)),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(24.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12.0),
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black12,
                                          offset: Offset(0, 3),
                                          blurRadius: 10)
                                    ],
                                  ),
                                  child: Column(
                                    children: [
                                      Icon(
                                        Icons.assignment_outlined,
                                        color: Colors.blue,
                                      ),
                                      Text(tSub.toString(),
                                          style: TextStyle(
                                              fontSize: 36,
                                              color: Color(0xFF011C63))),
                                      Text(
                                        'Courses',
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Color(0xFF011C63)),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(16.0),
                            padding: EdgeInsets.all(16.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12.0),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.black12,
                                    offset: Offset(0, 3),
                                    blurRadius: 10)
                              ],
                            ),
                            child: Column(
                              children: [
                                Text(
                                  "GPV/Course",
                                  style: TextStyle(
                                      fontSize: 12, color: Color(0xFF011C63)),
                                ),
                                SizedBox(
                                    width: MediaQuery.of(context).size.width *
                                        0.75,
                                    height:
                                        MediaQuery.of(context).size.width * 0.5,
                                    child: new charts.LineChart(
                                      series,
                                      animate: false,
                                    ))
                              ],
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(
                                vertical: 12.0, horizontal: 0.0),
                            padding: EdgeInsets.symmetric(
                                vertical: 8.0, horizontal: 0.0),
                            child: Text("Courses",
                                style: TextStyle(
                                    fontSize: 16.0, color: Color(0xFF011C63))),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12.0)),
                            child: SizedBox(
                              height: MediaQuery.of(context).size.height * 0.58,
                              child: ListView.builder(
                                  itemCount: list.length,
                                  itemBuilder: (cnt, indx) {
                                    return CourceCard(list[indx]);
                                  }),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      )),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blue,
        child: Icon(Icons.add),
        onPressed: () {
          showDialog(
              context: context,
              useSafeArea: true,
              barrierDismissible: false,
              builder: (BuildContext context) {
                return SimpleDialog(
                  title: Text(
                    'Add Subject ',
                    style: TextStyle(color: Color(0xFF011C63)),
                  ),
                  children: [
                    NewSubjectView(
                      isUpdate: false,
                      sub: null,
                    )
                  ],
                );
              });
        },
      ),
    );
  }

  void initState() {
    super.initState();
    SubjectDb.init().then((value) {
      db = value;
      SubjectDb.getSubjects(db).then((value) {
        setState(() {
          if (value.length > 0)
            list = value;
          else
            list = [];
        });
      }).catchError((onError) => print(onError));
    }).catchError((onError) => print(onError));
  }
}

class MyCliper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height * 0.5);
    path.quadraticBezierTo(
        size.width * 0.5, size.height * 1.5, size.width, size.height * 0.5);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}

class CardCliper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, 0);
    path.lineTo(size.width * 0.125, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}

class SubjectGPA {
  // ignore: non_constant_identifier_names
  int subject_code;
  double gpa;
  // ignore: non_constant_identifier_names
  SubjectGPA({this.subject_code, this.gpa});
}
